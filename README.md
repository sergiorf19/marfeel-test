# Test Marfeel - Frontend Developer
## Frontend Jedi

Project developed with JavaScript (ES6), Webpack, D3.js and Karma with Jasmine for the tests.

## Instructions for use
## Installation & Usage

- First of all, install with npm the necessary dependencies and modules.
    `npm install`

- To begin, you can deploy the application in a local environment on your own machine. with the command: 
    `npm start`

- On the other hand, you can create a directory with compiled files ready to be deployed to production. with the command: 
    `npm run build`

**IMPORTANT:**
### For testing

- To run the test battery.
    `npm run test`
