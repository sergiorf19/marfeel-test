const webpackConfig = require('./webpack.config.js');

module.exports = function (config) {
  config.set({
    webpack: webpackConfig,
    basePath: './',
    frameworks: ['jasmine'],
    
    files: [
      'test/test.js'
    ],
    preprocessors: {
      'test/test.js': ['webpack']
    },
    plugins: [
      require('karma-webpack'),
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-coverage-istanbul-reporter'),
      require('karma-jasmine-html-reporter')
    ],
    reporters: ['progress', 'kjhtml'],
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, 'coverage'), reports: ['html', 'lcovonly'],
      fixWebpackSourcePaths: true
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    failOnEmptyTestSuite: false,
    browsers: ['Chrome'],
    singleRun: false,
    concurrency: Infinity
  })
}
