// =====================================
// REQUIRES AND DEPENDENCIES
// =====================================

// Import Styles
import './styles.css';


// Import PieChart Template generator
import {createPieChartWrap} from './js/pie-chart.js';


// Import all Images from assets
function requireAll(r) { r.keys().forEach(r); }
requireAll(require.context('./assets/img', true));

// Import all JSON from data
import data from './data/mock.json';


// =====================================
// INIT APPLICATION - GETTING DATA
// =====================================

// GET DATA AND GENERATE CHARTS
function getChartData(dataFile) {
  const dataArr = dataFile.chartData;
  for (var i = 0; i < dataArr.length; ++i) {
    createPieChartWrap(dataArr[i], i);
  }
  return dataArr;
}
getChartData(data);



// =====================================
// SETTING VALUE FORMATS
// =====================================

const addDecimalPoints = (className) => {
  var $numberValue = document.getElementsByClassName(className);
  for (let text of $numberValue) {
    text.innerHTML = text.innerHTML.replace(/\D/g, '');
    var inputValue = text.innerHTML.replace('.', '').split("").reverse().join(""); // reverse
    var newValue = '';
    for (var i = 0; i < inputValue.length; i++) {
      if (i % 3 == 0 && i !== 0) { newValue += '.'; }
      newValue += inputValue[i];
    }
    text.innerHTML = newValue.split("").reverse().join("");
  }
};
addDecimalPoints('numberValue');