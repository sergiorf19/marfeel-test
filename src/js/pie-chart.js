// =====================================
// REQUIRES AND DEPENDENCIES
// =====================================

// Import D3.js Library
const d3 = require('d3');


// =====================================
// GENERATE PIE CHART
// =====================================

const createPieChartWrap = (data, i) => {
	const $chartSlider = document.querySelector("#chart-slider");
	var $node = document.createElement('div');
	$node.id = `chart-wrap${i + 1}`;
	$chartSlider.appendChild($node);
	
	generatePieChart(data, i);
}


const generatePieChart = (data, i) => {
	
	const $chartWrap = document.querySelector(`#chart-wrap${i + 1}`);
	$chartWrap.className = 'chart-wrap';
	
	// CONFIG CHART
	const chartDataConfig = {
		width: $chartWrap.offsetWidth,
		height: $chartWrap.offsetHeight,
		radius: Math.min($chartWrap.offsetWidth, $chartWrap.offsetHeight) / 2.4,
		innerRadius: Math.min($chartWrap.offsetWidth, $chartWrap.offsetHeight) / 2.6
	};
	
	const dataSelected = data;
	let deg = 73;
	if(i === 0){
		deg = 144;
	} else if(i === 1) {
		deg = 0;
	} else if(i === 2) {
		deg = 73;
	}


	// CONFIGURING COLORS FOR EVERY ARC
	let color = d3.scale.ordinal()
		.range(dataSelected.colors);


	// APPENDING TO VIEW AND CONFIG SIZE OF CHART
	let canvas = d3.select(`#chart-wrap${i + 1}`).append('svg')
		.attr('width', chartDataConfig.width)
		.attr('height', chartDataConfig.height)
		.attr('style', `transform: rotate(${deg}deg);`);

	let group = canvas.append('g')
		.attr('transform', `translate(${chartDataConfig.width / 2}, ${chartDataConfig.height / 2})`);


	// CONFIGURING RADIUS ARC WEIGHT
	let arc = d3.svg.arc()
		.innerRadius(chartDataConfig.innerRadius)
		.outerRadius(chartDataConfig.radius);


	// GENERATING THE PIE CHART WITH DATA
	let pie = d3.layout.pie()
		.value(d => d);

	let arcs = group.selectAll('.arc')
		.data(pie(dataSelected.data))
		.enter()
		.append('g')
		.attr('class', 'arc');


	// FILLING THE ARC WITH COLOR
	arcs.append('path')
		.attr('d', arc)
		.attr('fill', d => color(d.data));


	// CREATE AND APPEND INNER CHART CONTENT
	var $innerChart = document.createElement('div');
	$innerChart.className = 'inner-html';
	$innerChart.style.backgroundImage = `url('./assets/img/chart_bg${ i + 1 }.svg')`;
	$innerChart.innerHTML = `
		<div class="label">
			<h5>${dataSelected.label.toUpperCase()}</h5>
			<p>
				<span class="numberValue">${ dataSelected.total }</span>
				${dataSelected.type === 'euro' ? '<span class="append"> €</span>' : ''}
				${dataSelected.type === 'dollar' ? '<span class="append"> $</span>' : ''}
			</p>
		</div>
	`;
	$chartWrap.appendChild($innerChart);

	// CREATE AND APPEND SECONDARY INFO
	var $secondaryInfo = document.createElement('div');
	$secondaryInfo.className = 'secondary-info';
	$secondaryInfo.innerHTML = `
		<div class="tablet">
			<h5 style="color: ${dataSelected.colors[0]}">Tablet</h5>
			<p>${ (dataSelected.data[1] * 100) / dataSelected.total}% <span class="numberValue"> ${ dataSelected.data[1] + '  ' }</span></p>
		</div>
		<div class="smartphone">
			<h5 style="color: ${dataSelected.colors[1]}">Smartphone</h5>
			<p>${ (dataSelected.data[0] * 100) / dataSelected.total}% <span class="numberValue"> ${ '  ' + dataSelected.data[0] }</span></p>
		</div>
	`;
	$chartWrap.appendChild($secondaryInfo);

};

module.exports = { createPieChartWrap };