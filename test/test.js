const marfeelApp = require('../src/index.js');
import data from '../src/data/mock.json';

describe('MarfeelAppTest', function () {
  let fixture;
  beforeEach(function () {
    let body = document.getElementsByTagName("body")[0]; 
    fixture = `<div class="wrap">
        <div id="chart-slider"></div>
    </div>`;
    body.appendChild(fixture);
  });

  it('Should return a point every 3 digits', function () {
    const result = 2000000;
    marfeelApp.addDecimalPoints(result);
    expect(result).toBe('2.000.000');
  });

  it('Should get return array when load json file', function () {
    const arrResult = marfeelApp.getChartData(data);
    expect(arrResult.isArray()).toBeTruthy();
  });

});