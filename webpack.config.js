const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
	entry: './src/index.js',
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'dist')
	},
	devtool: 'eval-source-map',
	devServer: {
		contentBase: './dist'
	},
	plugins: [
		new UglifyJsPlugin({ sourceMap: true }),
		new CleanWebpackPlugin(['dist']),
		new HtmlWebpackPlugin({
			title: 'Marfeel Frontend Jedi',
			template: './src/index.html',
			filename: "./index.html",
			inject: 'body'
		})
	],
	module: {
		rules: [
			{
				test: /\.css$/,
				use: [
					'style-loader',
					'css-loader'
				]
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: "eslint-loader"
			},
			{
				test: /\.html$/,
				use: [
					{
						loader: "html-loader",
						options: { minimize: true }
					}
				]
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'assets/img'
					}
				}]
			},
			{
				type: 'javascript/auto',
				test: /\.json$/,
				use: [{
					loader: 'json-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'data'
					}
				}]
			},
			{
				test: /\.js$/,
				exclude: [
					/node_modules/,
					/spec/
				],
				loader: "babel-loader",
				options: {
					presets: ['es2015']
				}
			}
		]
	},
	mode: 'development'
};